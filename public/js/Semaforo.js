class Semaforo {
	constructor(selector) {
		this.selector = selector;	
		this.circle_green = $(selector + " .circle.green")[0];
		this.circle_yellow = $(selector + " .circle.yellow")[0];
		this.circle_red = $(selector + " .circle.red")[0];
		this.on = {
			"opacity" : "1"
		};
		this.off = {
			"opacity" : "0.3"
		}
	}
	setGreen() {
		/*$(this.circle_green).removeClass("off").addClass("on");
		$(this.circle_yellow).removeClass("on").addClass("off");
		$(this.circle_red).removeClass("on").addClass("off");*/
		$(this.circle_green).css(this.on);
		$(this.circle_yellow).css(this.off);
		$(this.circle_red).css(this.off);
	}
	setYellow() {
		$(this.circle_green).css(this.off);
		$(this.circle_yellow).css(this.on);
		$(this.circle_red).css(this.off);
	}
	setRed() {
		$(this.circle_green).css(this.off);
		$(this.circle_yellow).css(this.off);
		$(this.circle_red).css(this.on);
	}
	setOff() {
		$(this.circle_green).css(this.off);
		$(this.circle_yellow).css(this.off);
		$(this.circle_red).css(this.off);
	}
	standBy() {
		var _c = 0;
		var self = this;
		setInterval(function () {
			if(_c == 0) {
				self.setYellow();
				_c = 1;
			}
			else {
				self.setOff();
				_c = 0;
			}
		}, 1000);
	}
}