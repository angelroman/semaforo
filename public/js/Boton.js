class Boton {
  constructor(selector) {
    this.selector = selector;
    this.btnPlay = $(this.selector+" i");
  }
  setPlay() {
    this.btnPlay.removeClass("fa fa-pause").addClass("fa fa-play").css({
      left: "35px", top: "25px"
    });
    $(this.selector).css("background-color", "#3498db");
  }
  setStop() {
    this.btnPlay.removeClass("fa fa-play").addClass("fa fa-pause").css({
      left: "29px", top: "26px"
    });
    $(this.selector).css("background-color", "#c0392b");
  }
}