class Numero {
  constructor(selector) {
    this.a = $(selector + " .a");
    this.b = $(selector + " .b");
    this.c = $(selector + " .c");
    this.d = $(selector + " .d");
    this.e = $(selector + " .e");
    this.f = $(selector + " .f");
    this.g = $(selector + " .g");
  }
  setZero(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);
    this.d.removeClass("off").addClass("on").css("background-color", color);
    this.e.removeClass("off").addClass("on").css("background-color", color);
    this.f.removeClass("off").addClass("on").css("background-color", color);
    
    this.g.removeClass("on").addClass("off").css("background-color", color);
  }
  setOne(color) {
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);
    
    this.a.removeClass("on").addClass("off").css("background-color", color);
    this.d.removeClass("on").addClass("off").css("background-color", color);
    this.e.removeClass("on").addClass("off").css("background-color", color);
    this.f.removeClass("on").addClass("off").css("background-color", color);
    this.g.removeClass("on").addClass("off").css("background-color", color);
  }
  setTwo(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
    this.e.removeClass("off").addClass("on").css("background-color", color);
    this.d.removeClass("off").addClass("on").css("background-color", color);
    
    this.c.removeClass("on").addClass("off").css("background-color", color);    
    this.f.removeClass("on").addClass("off").css("background-color", color);
  }
  setThree(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);    
    this.d.removeClass("off").addClass("on").css("background-color", color);
    
    this.e.removeClass("on").addClass("off").css("background-color", color);
    this.f.removeClass("on").addClass("off").css("background-color", color);
  }
  setFour(color) {
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);    
    this.f.removeClass("off").addClass("on").css("background-color", color);
    
    this.a.removeClass("on").addClass("off").css("background-color", color);
    this.e.removeClass("on").addClass("off").css("background-color", color);
    this.d.removeClass("on").addClass("off").css("background-color", color);
  }
  setFive(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.f.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);
    this.d.removeClass("off").addClass("on").css("background-color", color);
    
    this.b.removeClass("on").addClass("off").css("background-color", color);
    this.e.removeClass("on").addClass("off").css("background-color", color);
  }
  setSix(color) {
    this.f.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);    
    this.d.removeClass("off").addClass("on").css("background-color", color);
    this.e.removeClass("off").addClass("on").css("background-color", color);
    
    this.a.removeClass("on").addClass("off").css("background-color", color);
    this.b.removeClass("on").addClass("off").css("background-color", color);
  }
  setSeven(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);
    
    this.g.removeClass("on").addClass("off").css("background-color", color);
    this.d.removeClass("on").addClass("off").css("background-color", color);
    this.e.removeClass("on").addClass("off").css("background-color", color);
    this.f.removeClass("on").addClass("off").css("background-color", color);
  }
  setEight(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);
    this.d.removeClass("off").addClass("on").css("background-color", color);
    this.e.removeClass("off").addClass("on").css("background-color", color);
    this.f.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
  }
  setNine(color) {
    this.a.removeClass("off").addClass("on").css("background-color", color);
    this.b.removeClass("off").addClass("on").css("background-color", color);
    this.c.removeClass("off").addClass("on").css("background-color", color);
    this.f.removeClass("off").addClass("on").css("background-color", color);
    this.g.removeClass("off").addClass("on").css("background-color", color);
    
    this.d.removeClass("on").addClass("off").css("background-color", color);
    this.e.removeClass("on").addClass("off").css("background-color", color);
  }
  setOff(color) {
    this.a.removeClass("on").addClass("off").css("background-color", color);
    this.b.removeClass("on").addClass("off").css("background-color", color);
    this.c.removeClass("on").addClass("off").css("background-color", color);
    this.f.removeClass("on").addClass("off").css("background-color", color);
    this.g.removeClass("on").addClass("off").css("background-color", color);
    
    this.d.removeClass("on").addClass("off").css("background-color", color);
    this.e.removeClass("on").addClass("off").css("background-color", color);
  } 
}

class Clock {
  constructor(leftSelector, rightSelector) {
    this.numberLeft = new Numero(leftSelector);
    this.numberRight = new Numero(rightSelector);
  }
  getZero(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setZero(color);
  }
  getOne(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setOne(color);
  }
  getTwo(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setTwo(color);
  }
  getThree(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setThree(color);
  }
  getFour(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setFour(color);
  }
  getFive(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setFive(color);
  }
  getSix(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setSix(color);
  }
  getSeven(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setSeven(color);
  }
  getEight(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setEight(color);
  }
  getNine(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setNine(color);
  }
  getTen(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setZero(color);
  }
  getEleven(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setOne(color);
  }
  getTwelve(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setTwo(color);
  }
  getThirteen(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setThree(color);
  }
  getFourteen(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setFour(color);
  }
  getFifteen(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setFive(color);
  }
  getSixteen(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setSix(color);
  }
  getSeventeen(color) {
    this.numberLeft.setOne(color);
    this.numberRight.setSeven(color);
  }
  standby(color) {
    this.numberLeft.setZero(color);
    this.numberRight.setZero(color);
  }
  getOff(color) {
    this.numberLeft.setOff(color);
    this.numberRight.setOff(color);
  }
}