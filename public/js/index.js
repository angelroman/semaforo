$(document).ready(function () {
  /*
    7 seg verde
    2 seg amarilllo
    7 seg rojo

    amarillo parpadea 2 veces
    verde parpadea 3 veces
  */

  let main = null;
  setTimeout(function () {
    main = new Main();
    main.btnPlay.click(function () {
      if(main.play) {
        //Detener el semaforo
        main.btnPlayStop.setPlay();
        main.play = false;
        clearInterval(main.timmer);
      }
      else {  
        //Iniciar el semaforo
        main.btnPlayStop.setStop();
        main.play = true;
        main.cicloUnSegundo();
      }
    });
  }, 500);

  var btnRestart = $("#btnRestart");
  btnRestart.click(function () {
    console.log("Reiniciar");
    clearInterval(main.timmer);
    main._num = 0;
    main.play = true;
    main.turno = 0;
    main.btnPlayStop.setStop();
    main.reloj.getZero(main.standby);
    main.semaforoIzquierdo.setRed();
    main.semaforoArriba.setRed();
    main.semaforoDerecho.setRed();
    main.semaforoAbajo.setRed();

    main.cicloUnSegundo();
  });

  var btnIntermitente = $("#intermitente");
  btnIntermitente.click(function () {
    console.log("ClickIntermitenmte");
    clearInterval(main.timmer);
    main.num = 0;
    main.turno = 0;
    main.btnPlayStop.setPlay();
    main.play = false;
    clearInterval(main.timmer);
    main.Intermitente();
  });

});
class Main {
  constructor() {
    this.verde        = "#2ecc71";
    this.amarillo     = "#f1c40f";
    this.rojo         = "#c0392b";
    this.standby      = "#d35400";

    this._num         = 0;
    this.timmer       = null;
    this.play         = false;
    this.btnPlay      = $("#btnPlay");

    this.btnPlayStop          = new Boton("#btnPlay");
    this.reloj                = new Clock("#number-left", "#number-rigth");
    this.semaforoIzquierdo    = new Semaforo("#semaforo-izquierdo");
    this.semaforoDerecho      = new Semaforo("#semaforo-derecho");
    this.semaforoArriba       = new Semaforo("#semaforo-arriba");
    this.semaforoAbajo        = new Semaforo("#semaforo-abajo");

    this.turno = 0;

    this.Semaforos = [this.semaforoIzquierdo, this.semaforoDerecho, this.semaforoArriba, this.semaforoAbajo];
    this.contadorTurno = 0;

    this.contadorAmarillos = 0;
  }
  Intermitente() {
    var self = this;
    var self = this;
    self._num =0;
    clearInterval(self.timmer);
    self.timmer = setInterval(function () {
      self.VueltaAmarillos();
    }, 500);
  }
  IntermitenteApagado() {
    var self = this;
    self.semaforoIzquierdo.setOff();
    self.semaforoArriba.setOff();
    self.semaforoDerecho.setOff();
    self.semaforoAbajo.setOff();
    self.reloj.getOff("yellow");
    socket.emit('apagar', 'todos');
  }
  IntermitenteEncendido() {
    var self = this;
    self.semaforoIzquierdo.setYellow();
    self.semaforoArriba.setYellow();
    self.semaforoDerecho.setYellow();
    self.semaforoAbajo.setYellow();
    self.reloj.getZero("yellow");
    socket.emit('amarillos');
  }
  VueltaAmarillos() {
    if(this.contadorAmarillos == 0) {
      this.contadorAmarillos = 1;
      this.IntermitenteApagado();
    }
    else {
      this.contadorAmarillos = 0;
      this.IntermitenteEncendido();
    }
  }

  Vuelta() {
    var self = this;
    console.log(self._num);
    switch(self._num) {
      case 0://0
        self.reloj.getZero(self.standby);
        self.Turno();
        self._num++;
        break;
      case 1://1
        self.reloj.getOne(self.verde);
        self.Turno();
        self._num++;
        break;
      case 2://2
        self.reloj.getTwo(self.verde);
        self.Turno();
        self._num++;
        break;
      case 3: //3
        self.reloj.getThree(self.verde);
        self.Turno();
        self._num++;
        break;
      case 4://4
        self.reloj.getFour(self.verde);
        self.Turno();
        self._num++;
        break;
      case 5://5
        self.reloj.getFive(self.verde);
        self.Turno();
        self._num++;
        break;
      case 6://6
        self.reloj.getSix(self.verde);
        self.Turno();
        self._num++;
        break;
      case 7://7
        self.reloj.getSeven(self.verde);
        self.Turno();
        self.cicloMedioSegundo();
        self._num++;
        break;
      case 8://7.5
        self.reloj.getSeven(self.verde);
        self.Turno();
        self._num++;
        break;
      case 9://1
        self.reloj.getOne(self.verde);
        self.Turno();
        self._num++;
        break;
      case 10://1.5
        self.reloj.getOne(self.verde);
        self.Turno();
        self._num++;
        break;
      case 11://2
        self.reloj.getTwo(self.verde);
        self.Turno();
        self._num++;
        break;
      case 12://2.5
        self.reloj.getTwo(self.verde);
        self.Turno();
        self._num++;
        break;
      case 13://3
        self.reloj.getThree(self.verde);
        self.Turno();
        self._num++;
        break;
      case 14://3.5
        self.reloj.getThree(self.verde);
        self.Turno();
        self._num++;
        break;
      case 15://1
        self.reloj.getOne(self.amarillo);
        self.cicloUnSegundo();
        self.Turno();
        self._num++;
        break;
      case 16://2
        self.reloj.getTwo(self.amarillo);
        self.Turno();
        self._num++;
        break;
      case 17://1
        self.reloj.getOne(self.rojo);
        self.Turno();
        self._num++;
        break;
      case 18://2
        self.reloj.getTwo(self.rojo);
        self.Turno();
        self._num = 1;
        self.CambiaTurno();
        break;
      /*case 16://2
        debugger;
        self.reloj.getTwo(self.rojo);
        self.Turno();
        self._num = 1;
        self.CambiaTurno();
        break;*/
      /*case 16://2
        self.reloj.getTwo(self.rojo);
        self.CambiaTurno();
        self._num = 0;
        break;*/
      /*case 17://2
        self.reloj.getTwo(self.rojo);
        self.CambiaTurno();
        self._num++;
        break;*/
      /*case 17://11
        self.reloj.getEleven(self.rojo);
        self.Turno();
        self.CambiaTurno();
        self._num = 0;
        break;*/
    }
  }
  cicloMedioSegundo() {
    var self = this;
    clearInterval(self.timmer);
    self.timmer = setInterval(function () {
      self.Vuelta();
    }, 500);
  }
  cicloUnSegundo() {
    var self = this;
    clearInterval(self.timmer);
    self.timmer = setInterval(function () {
     self.Vuelta();  
    }, 1000);
  }
  Turno() {
    if(this.turno == 0) {
      this.semaforoArriba.setRed();
      this.semaforoAbajo.setRed();
      this.cicloSemaforoIzquierdo();
      this.cicloSemaforoDerecho();
    }
    if(this.turno == 1) {
      this.semaforoIzquierdo.setRed();
      this.semaforoDerecho.setRed();
      this.cicloSemaforoArriba();
      this.cicloSemaforoAbajo();
    }
    /*if(this.turno == 2) {
      this.cicloSemaforoDerecho();
    }
    if(this.turno == 3) {
      this.cicloSemaforoAbajo();
    }*/
  }
  CambiaTurno() {
    if(this.turno == 0) {
      this.turno = 1;
    } else if(this.turno == 1) {
      this.turno = 0;
    } /*else if(this.turno == 2) {
      this.turno = 3;
    } else if(this.turno == 3) {
      this.turno = 0;
    }*/
  }
  cicloSemaforoIzquierdo() {
    /*this.semaforoDerecho.setRed();
    this.semaforoArriba.setRed();
    this.semaforoAbajo.setRed();*/
    switch(this._num) {
      case 0: //0
        socket.emit('rojoCrucero1');
        this.semaforoIzquierdo.setRed();
        //socket.emit('rojoCrucero2');
        break;
      case 1://1
        socket.emit('verdeCrucero1');
        this.semaforoIzquierdo.setGreen();
        //socket.emit('rojoCrucero2');
        break;
      case 2://2
      socket.emit('verdeCrucero1');
        break;
      case 3://3
      socket.emit('verdeCrucero1');
        break;
      case 4://4
      socket.emit('verdeCrucero1');
        break;
      case 5://5
      socket.emit('verdeCrucero1');
        break;
      case 6://6
      socket.emit('verdeCrucero1');
        break;
      case 7://7
      socket.emit('verdeCrucero1');
        break;
      case 8://7.5
        socket.emit('apagar', 'crucero1');
        this.semaforoIzquierdo.setOff();
        break;
      case 9://1
      socket.emit('verdeCrucero1');
        this.semaforoIzquierdo.setGreen();
        break;
      case 10://1.5
      socket.emit('apagar', 'crucero1');
        this.semaforoIzquierdo.setOff();
        break;
      case 11://2
      socket.emit('verdeCrucero1');
        this.semaforoIzquierdo.setGreen();
        break;
      case 12://2.5
      socket.emit('apagar', 'crucero1');
        this.semaforoIzquierdo.setOff();
        break;
      case 13://3
      socket.emit('verdeCrucero1');
        this.semaforoIzquierdo.setGreen();
        break;
      case 14://3.5
      socket.emit('apagar', 'crucero1');
        this.semaforoIzquierdo.setOff();
        break;
      case 15://1
      socket.emit('amarilloCrucero1');
        this.semaforoIzquierdo.setYellow();
        break;
      case 16://2
      socket.emit('amarilloCrucero1');
        this.semaforoIzquierdo.setYellow();
        break;
      case 17://1
      socket.emit('rojoCrucero1');
      //socket.emit('rojoCrucero2');
        this.semaforoIzquierdo.setRed();
        break;
      case 18://2
      socket.emit('rojoCrucero1');
      //socket.emit('rojoCrucero2');
        this.semaforoIzquierdo.setRed();
        break;

      /*case 16://10
        this.semaforoIzquierdo.setRed();
        break;
      case 17://11
        //this.semaforoIzquierdo.setRed();
        break;*/
    }
  }
  cicloSemaforoArriba() {
    /*this.semaforoIzquierdo.setRed();
    this.semaforoDerecho.setRed();
    this.semaforoAbajo.setRed();*/
    switch(this._num) {
      case 0:
      socket.emit('rojoCrucero2');
      //socket.emit('rojoCrucero1');
        this.semaforoArriba.setRed();
        break;
      case 1:
      socket.emit('verdeCrucero2');
        this.semaforoArriba.setGreen();
        break;
      case 2:
      socket.emit('verdeCrucero2');
        break;
      case 3:
      socket.emit('verdeCrucero2');
        break;
      case 4:
      socket.emit('verdeCrucero2');
        break;
      case 5:
      socket.emit('verdeCrucero2');
        break;
      case 6:
      socket.emit('verdeCrucero2');
        break;
      case 7:
      socket.emit('verdeCrucero2');
        break;
      case 8://7.5
      socket.emit('apagar', 'crucero2');
        this.semaforoArriba.setOff();
        break;
      case 9://1
      socket.emit('verdeCrucero2');
        this.semaforoArriba.setGreen();
        break;
      case 10://1.5
      socket.emit('apagar', 'crucero2');
        this.semaforoArriba.setOff();
        break;
      case 11://2
      socket.emit('verdeCrucero2');
        this.semaforoArriba.setGreen();
        break;
      case 12://2.5
      socket.emit('apagar', 'crucero2');
        this.semaforoArriba.setOff();
        break;
      case 13://3
      socket.emit('verdeCrucero2');
        this.semaforoArriba.setGreen();
        break;
      case 14://3.5
      socket.emit('apagar', 'crucero2');
        this.semaforoArriba.setOff();
        break;
      case 15://1
      socket.emit('amarilloCrucero2');
        this.semaforoArriba.setYellow();
        break;
      case 16://2
      socket.emit('amarilloCrucero2');
        this.semaforoArriba.setYellow();
        break;
      case 17://1
      socket.emit('rojoCrucero2');
      //socket.emit('rojoCrucero1');
        this.semaforoArriba.setRed();
        break;
      case 18://2
      socket.emit('rojoCrucero2');
      //socket.emit('rojoCrucero1');
        this.semaforoArriba.setRed();
        break;
    }
  }
  cicloSemaforoDerecho() {
    /*this.semaforoIzquierdo.setRed();
    this.semaforoArriba.setRed();
    this.semaforoAbajo.setRed();*/
    switch(this._num) {
      case 0:
      socket.emit('rojoCrucero1');
      //socket.emit('rojoCrucero2');
        this.semaforoDerecho.setRed();
        break;
      case 1:
      socket.emit('verdeCrucero1');
        this.semaforoDerecho.setGreen();
        break;
      case 2:
      socket.emit('verdeCrucero1');
        break;
      case 3:
      socket.emit('verdeCrucero1');
        break;
      case 4:
      socket.emit('verdeCrucero1');
        break;
      case 5:
      socket.emit('verdeCrucero1');
        break;
      case 6:
      socket.emit('verdeCrucero1');
        break;
      case 7:
      socket.emit('verdeCrucero1');
        break;
      case 8://7.5
      socket.emit('apagar', 'crucero1');
        this.semaforoDerecho.setOff();
        break;
      case 9://1
      socket.emit('verdeCrucero1');
        this.semaforoDerecho.setGreen();
        break;
      case 10://1.5
      socket.emit('apagar', 'crucero1');
        this.semaforoDerecho.setOff();
        break;
      case 11://2
      socket.emit('verdeCrucero1');
        this.semaforoDerecho.setGreen();
        break;
      case 12://2.5
      socket.emit('apagar', 'crucero1');
        this.semaforoDerecho.setOff();
        break;
      case 13://3
      socket.emit('verdeCrucero1');
        this.semaforoDerecho.setGreen();
        break;
      case 14://3.5
      socket.emit('apagar', 'crucero1');
        this.semaforoDerecho.setOff();
        break;
      case 15://1
      socket.emit('amarilloCrucero1');
        this.semaforoDerecho.setYellow();
        break;
      case 16://2
      socket.emit('amarilloCrucero1');
        this.semaforoDerecho.setYellow();
        break;
      case 17://1
      socket.emit('rojoCrucero1');
      //socket.emit('rojoCrucero2');
        this.semaforoDerecho.setRed();
        break;
      case 18://2
      socket.emit('rojoCrucero1');
      //socket.emit('rojoCrucero2');
        this.semaforoDerecho.setRed();
        break;
    }
  }
  cicloSemaforoAbajo() {
    /*this.semaforoIzquierdo.setRed();
    this.semaforoArriba.setRed();
    this.semaforoDerecho.setRed();*/
    switch(this._num) {
      case 0:
        this.semaforoAbajo.setRed();
        socket.emit('rojoCrucero2');
        //socket.emit('rojoCrucero1');
        break;
      case 1:
      socket.emit('verdeCrucero2');
        this.semaforoAbajo.setGreen();
        break;
      case 2:
      socket.emit('verdeCrucero2');
        break;
      case 3:
      socket.emit('verdeCrucero2');
        break;
      case 4:
      socket.emit('verdeCrucero2');
        break;
      case 5:
      socket.emit('verdeCrucero2');
        break;
      case 6:
      socket.emit('verdeCrucero2');
        break;
      case 7:
      socket.emit('verdeCrucero2');
        break;
      case 8://7.5
      socket.emit('apagar', 'crucero2');
        this.semaforoAbajo.setOff();
        break;
      case 9://1
      socket.emit('verdeCrucero2');
        this.semaforoAbajo.setGreen();
        break;
      case 10://1.5
      socket.emit('apagar', 'crucero2');
        this.semaforoAbajo.setOff();
        break;
      case 11://2
      socket.emit('verdeCrucero2');
        this.semaforoAbajo.setGreen();
        break;
      case 12://2.5
      socket.emit('apagar', 'crucero2');
        this.semaforoAbajo.setOff();
        break;
      case 13://3
      socket.emit('verdeCrucero2');
        this.semaforoAbajo.setGreen();
        break;
      case 14://3.5
      socket.emit('apagar', 'crucero2');
        this.semaforoAbajo.setOff();
        break;
      case 15://1
      socket.emit('amarilloCrucero2');
        this.semaforoAbajo.setYellow();
        break;
      case 16://2
      socket.emit('amarilloCrucero2');
        this.semaforoAbajo.setYellow();
        break;
      case 17://1
      socket.emit('rojoCrucero2');
      //socket.emit('rojoCrucero1');
        this.semaforoAbajo.setRed();
        break;
      case 18://2
      socket.emit('rojoCrucero2');
      //socket.emit('rojoCrucero1');
        this.semaforoAbajo.setRed();
        break;
    }
  }
}