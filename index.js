var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var lpt = require('lpt');
//var port = new lpt.Port(0);
var port = new lpt.Port(0);
port.data = 64;
port.control.init = false;
app.use(express.static(__dirname + '/public'));
app.get('/', function (req, res) {
	res.sendfile('index.html');
});
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('chat message', function(msg){
      console.log('message: ' + msg);
      io.emit('chat message', msg);

      console.log('port.status.ack: ' + port.status.ack);//port.status.ack = false;
      console.log('port.status.busy: ' + port.status.busy);//port.status.busy = false;
      console.log('port.status.select: ' + port.status.select);//port.status.select = false;
      console.log('Data: ' + port.data);
      console.log('Busy:' + port.status.busy);
    });
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  socket.on("intermitente", function () {
    fnAmarillo();
  });
  socket.on('amarillos', function () {
    fnAmarillo();
  });
  

  socket.on('verdeCrucero1', function(resp) {
  	//verdeCrucero1();
    fnVerdeRojoCrucero1();
    //fnAmarilloRojoCrucero1();
  });
  socket.on('amarilloCrucero1', function(resp) {
  	fnAmarilloRojoCrucero1();
    //amarilloCrucero1();
    //fnAmarilloRojoCrucero2();
    //fnAmarilloRojoCrucero1();
  });
  socket.on('rojoCrucero1', function(resp) {
    //fnVerdeRojoCrucero1();
  	//rojoCrucero1();
    TodosRojos();
    //fnAmarilloRojoCrucero1();
  });

  socket.on('verdeCrucero2', function(resp) {
    fnVerdeRojoCrucero2();
  	//verdeCrucero2();
    //fnVerdeRojoCrucero1();
    //fnAmarilloRojoCrucero1();
  });
  socket.on('amarilloCrucero2', function(resp) {
    fnAmarilloRojoCrucero2();
  	//amarilloCrucero2();
    //fnAmarilloRojoCrucero1();
    //fnAmarilloRojoCrucero1();
  });
  socket.on('rojoCrucero2', function(resp) {
    //fnVerdeRojoCrucero1();
  	//rojoCrucero2();
    TodosRojos();
    //fnAmarilloRojoCrucero1();
  });
  var fnAmarillo = function () {
    port.data = 36;
  };
  var fnVerdeRojoCrucero2 = function () {
    port.data = 10;
  };
  var fnVerdeRojoCrucero1 = function () {
    port.data = 17;
  };
  //Amarillo crucero 2 y rojo crucero 1
  var fnAmarilloRojoCrucero2 = function () {
    port.data = 34;
  };
  //Amarillo crucero 1 y rojo crucero 2
  var fnAmarilloRojoCrucero1 = function () {
    port.data = 20;
  };
  var fnApagados = function (resp) {
    port.data = 1;
  };

  socket.on('apagar', function(resp) {
  	apagar(resp);
  });

});
http.listen(3000, function () {
	console.log("Escuchando en el puerto 3000");
});

var TodosRojos = function () {
  port.data = 18;
};

var verdeCrucero1 = function () {
	console.log("Prender verdes el curcero 2");
	port.data = 8;
};
var amarilloCrucero1 = function () {
	console.log("Prender verdes el curcero 2");
	port.data = 32;
};
var rojoCrucero1 = function () {
	console.log("Prender verdes el curcero 2");
	port.data = 16;
};


var verdeCrucero2 = function () {
	console.log("Prender verdes el curcero 2");
	port.data = 1;
};
var amarilloCrucero2= function () {
	console.log("Prender verdes el curcero 2");
	port.data = 4;
};
var rojoCrucero2 = function () {
	console.log("Prender verdes el curcero 2");
	port.data = 2;
};

var apagar = function (resp) {
  console.log("RESP:  >>> " + resp);
  if(resp == 'crucero1') {
    port.data = 16;
  }
  else if(resp == 'crucero2'){
    port.data = 2;
  }
  else if(resp == 'todos') {
    port.data = 0;
  }
};


